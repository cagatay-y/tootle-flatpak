# tootle-flatpak
Flatpak manifest for [Tootle](https://github.com/bleakgrey/tootle), a GTK+ Mastodon client.

Please use the [Flathub package](https://flathub.org/apps/details/com.github.bleakgrey.tootle) for the stable version

CI builds from `master` branch of the Tootle repo and is scheduled to build every week (Sundays at 4:00am).

## Installation
1. Download [the latest build](https://gitlab.com/cagatay-y/tootle-flatpak/-/jobs/artifacts/master/download?job=flatpak_bundle)
2. Unzip the download.
3. Install the bundle
    - from command line with `flatpak install tootle.flatpak`
    - or from your app center (e.g. GNOME Software) by double clicking.

## Updating
Since the installation is a single file bundle, it cannot be updated automatically. To update, manually reinstall.

## Build
If you prefer to build yourself, run
```bash
flatpak-builder <build-dir> com.github.bleakgrey.tootle.json --install
```
You can find more information about building Flatpaks from [`flatpak-builder` docs](http://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html).
